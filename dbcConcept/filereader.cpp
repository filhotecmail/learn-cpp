/*
  Projeto: Learn C++ | Advanced Topics
  Arquivo: filereader.cpp
  Email: filhotecmail@gmail.com
  Analista: Carlos Dias da Silva Filho

  Objetivo do Programa:
  - A classe FileReader realiza a leitura de um arquivo e emite eventos associados ao processo.
  - Implementa a interface IFileReader para garantir a consistência na utilização de callbacks e eventos.

  Paradigmas e Técnicas:
  - Utilização de callbacks (funções de retorno de chamada) para manipular a estrutura de informações do arquivo.
  - Exploração do paradigma de programação funcional com o uso de funções lambda como callbacks.

  Bibliotecas Utilizadas:
  - <iostream>: Biblioteca padrão de entrada/saída. Utilizada para exibir mensagens no console.
  - "ifilereader.h": Arquivo de cabeçalho que contém a interface IFileReader.
  - <thread>: Biblioteca que fornece suporte para operações com threads. Usada para introduzir pausas temporárias.
  - <chrono>: Biblioteca de utilidades de tempo. Utilizada para definir pausas temporárias com precisão.
  - <fstream>: Biblioteca de fluxo de arquivo. Utilizada para operações de leitura e gravação em arquivos.

 TODO:
  - Implementar a leitura do arquivo de forma assíncrona para melhorar a concorrência de dados.
  - Adicionar tratamentos de erros mais abrangentes para manipular diversas situações de falha na leitura do arquivo.
  - Incluir mecanismos para lidar com operações I/O, como abrir, fechar, e manipular o cursor do arquivo.
  - Adotar práticas de concorrência de dados para garantir a segurança em ambientes multithread.
  - Expandir os eventos e callbacks para cobrir casos específicos durante a leitura do arquivo.
  - Considerar a implementação de funcionalidades adicionais, como leitura de diferentes tipos de arquivo.
  - Realizar testes rigorosos para validar o comportamento da classe em diferentes cenários.
  - Documentar de maneira mais detalhada os métodos e decisões de design para facilitar a manutenção futura.
  - Avaliar e otimizar o desempenho da classe para garantir eficiência em larga escala.

  Nota: Este comentário serve como cabeçalho para o arquivo filereader.cpp, fornecendo uma visão geral do propósito
        e estilo de implementação da classe FileReader.

        Não está completo, e não tem ainda as implementações e tratamentos de erros necessários


*/

#include <iostream>
#include "ifilereader.h"
#include <thread>
#include <chrono>
#include <fstream>


/**
 * @brief Estrutura que define mensagens de sinalização para o processo.
 *
 * A estrutura `SignalsMessage` contém constantes de string que representam mensagens associadas ao processo,
 * como mensagens de início, sucesso, leitura de arquivo e mensagens de erro.
 *
 * @note Membros de Dados:
 * - @MSGC00A: Mensagem indicando que o processo está sendo preparado.
 * - @MSGC00B: Mensagem indicando que o processo foi preparado com sucesso e a leitura do arquivo está prestes a começar.
 * - @MSG00C: Mensagem indicando que o arquivo está sendo lido.
 * - @MSG00D: Mensagem indicando que o processo foi concluído com sucesso.
 * - @MSG00E: Mensagem indicando um erro ao abrir o arquivo.
 */
struct SignalsMessage {

    const std::string MSGC00A = "ok, Vou começar a preparar as coisas... ";
    const std::string MSGC00B = "processo preparado com sucesso, aguarde que vou começar a executar a leitura do file ";
    const std::string MSG00C = "Lendo arquivo...";
    const std::string MSG00D = "Processo terminado com sucesso";
    const std::string MSG00E = "Erro ao abrir o arquivo";
};

/**
 * @brief Classe FileReader que implementa a interface IFileReader.
 *
 * A classe `FileReader` é responsável por realizar a leitura de um arquivo e emitir eventos associados ao processo.
 * Implementa a interface `IFileReader` para garantir a consistência na utilização.
 *
 * @note Membros de Dados:
 * - @finfo Estrutura que armazena informações relacionadas ao arquivo.
 * - @messages Estrutura que define mensagens de sinalização para o processo.
 *
 * @note Construtor Padrão:
 * - @FileReader(): Construtor padrão que inicializa uma instância da classe `FileReader`.
 *
 * @note Métodos Públicos:
 * - @loadFile: Implementação do método da interface que carrega informações do arquivo e executa eventos associados.
 * - @startJob: Implementação do método da interface que inicia o processo e emite eventos associados.
 * - @Build: Método estático que cria uma instância de `FileReader` para iniciar a construção do objeto.
 */
class FileReader : public IFileReader {
private:

    FileInInfo finfo;
    SignalsMessage messages;
public:
    FileReader() = default;

    /**
     * @brief Método da interface que carrega informações do arquivo e executa eventos associados.
     *
     * O método `loadFile` aceita uma função de retorno de chamada que manipula a estrutura `FileInInfo`.
     * Após a execução da função de retorno de chamada, o método tenta abrir o arquivo e, em caso de falha, emite um evento
     * de erro. Em seguida, são realizados eventos de pré-leitura antes de retornar a instância de `FileReader`.
     *
     * @param callback Função de retorno de chamada para manipular `FileInInfo`.
     * @return Referência para a instância de `FileReader`.
     */
    IFileReader &loadFile(const std::function<void(FileInInfo &)> &callback) override {

        callback(finfo);

        std::ifstream file(finfo.filename);

        if (!file.is_open()) {
            auto localError = FileInInfo::ErrorData(FileInInfo::ErrorData::ErrorType::ERROR,
                                                    messages.MSG00E,
                                                         500);
            if (finfo.events.onerror) {
                finfo.events.onerror( localError );
            }

            abort();

        }

        std::this_thread::sleep_for(std::chrono::seconds(2));
        if (finfo.events.beforeRead) { finfo.events.beforeRead(messages.MSGC00A); }

        std::this_thread::sleep_for(std::chrono::seconds(2));
        if (finfo.events.beforeRead) { finfo.events.beforeRead(messages.MSGC00B); }

        return *this;
    }

    /**
     * @brief Método da interface que inicia o processo e emite eventos associados.
     *
     * O método `startJob` realiza eventos associados à execução do processo, como eventos de pré-leitura e pós-leitura.
     *
     * @return Referência para a instância de `FileReader`.
     */
    IFileReader &startJob() override {

        std::this_thread::sleep_for(std::chrono::seconds(2));

        if (finfo.events.beforeRead) {
            finfo.events.beforeRead(messages.MSG00C);
        }

        std::this_thread::sleep_for(std::chrono::seconds(2));

        if (finfo.events.afterRead) {
            finfo.events.afterRead(messages.MSG00D);
        }

        return *this;
    }

    /**
     * @brief Método estático para criar uma instância de `FileReader`.
     *
     * O método estático `Build` é utilizado para iniciar a construção do objeto `FileReader`.
     *
     * @return Instância de `FileReader`.
     */
    static FileReader Build() {
        return FileReader();
    }
};