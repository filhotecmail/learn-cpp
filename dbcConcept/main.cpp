/*
  Projeto: Learn C++ | Advanced Topics
  Arquivo: main.cpp
  Email: filhotecmail@gmail.com
  Analista: Carlos Dias da Silva Filho

  Objetivo do Programa:
  - O programa utiliza a classe FileReader para realizar operações de leitura de arquivo com callbacks antes,
    durante e após a leitura.
  - A estrutura de callbacks permite personalizar o comportamento do programa em diferentes etapas do
    processamento do arquivo.

  Paradigmas e Técnicas:
  - O programa utiliza callbacks para realizar ações específicas em diferentes pontos do processamento do arquivo.
    Técnica essa que permite centralizar todas as ocorrências em forma de Assinaturas de Eventos.
    Evitando sair de dentro do escopo do Objeto, não necessitando capturar a referência por fora da costrução.

  - O paradigma de programação funcional é explorado através do uso de funções lambda como callbacks,
    não sei bem se é funcinal,  mas abuso um pouco disso para tentar devolver sempre a referência do proprio objeto.

  - A estrutura de erro é modelada como uma sub-struct dentro da struct FileInInfo, demonstrando encapsulamento
    e modularidade.

  Nota: Este comentário serve como cabeçalho para o arquivo main.cpp e fornece uma visão geral do propósito e
        estilo de implementação do programa.
*/
#include <iostream>
#include "filereader.cpp"

int main() {

    FileReader::Build()
            .loadFile([](FileInInfo &info) {

                info.filename = "Test de arquivo";

                info.events.beforeRead =  [&](const std::string &arg) {
                std::cout << "BeforeRead callback. Disparado: " << arg << std::endl;
            };

                info.events.afterRead =  [&](const std::string &arg) {
                std::cout << "AfterRead callback. Disparado: " << arg << std::endl;
            };

               //Por exemplo aqui, eu não precisaria capturar tudo , mas apenas o que interessa é a estrutura de erro
               // mas a Estrutura de erro é uma Sub struct dentro de info struct
               //
               info.events.onerror =  [&](const FileInInfo::ErrorData &error) {
                std::cout << "OnError callback. Disparado: ";
                std::cout << "Error Type: " << static_cast<int>(error.errorType) << std::endl;
                std::cout << "Error Message: " << error.errorMessage << std::endl;
                std::cout << "Error Code: " << error.errorCode << std::endl;
               };

            }).startJob();

    return 0;

}