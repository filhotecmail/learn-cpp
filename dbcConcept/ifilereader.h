//
// Created by f95 on 12/23/23.
//

#ifndef LEARN_C_ADVANCED_COURSE_IFILEREADER_H
#define LEARN_C_ADVANCED_COURSE_IFILEREADER_H

#include <string>
#include <functional>

/**
 * @brief Struct que armazena informações relacionadas a um arquivo.
 *
 * Esta struct é aninhada para armazenar dados de erro associados ao arquivo.
 * A estrutura `FileInInfo` representa informações associadas a um arquivo, como nome do arquivo, dados de erro e
 * eventos relacionados à leitura do arquivo.
 *
 * @note Membros de Dados:
 * - @filename Uma string que armazena o nome do arquivo.
 * - @errors Uma estrutura aninhada que encapsula informações de erro associadas ao arquivo.
 *    - @errorType Enumeração que define o tipo de erro, podendo ser `WARNING`, `ERROR` ou `FATAL`.
 *    - @errorMessage Uma string contendo a mensagem de erro.
 *    - @errorCode Um número inteiro que identifica o código de erro.
 *
 * - @events: Uma estrutura aninhada que guarda eventos relacionados à leitura do arquivo.
 *    - @beforeRead: Uma função que aceita uma string como argumento, representando um evento antes da leitura do arquivo.
 *    - @afterRead: Uma função que aceita uma string como argumento, representando um evento após a leitura do arquivo.
 *    - @onerror: Uma função que aceita a estrutura `ErrorData` como argumento, representando um evento de
 *    erro durante a leitura do arquivo.
 *
 * @note Construtor Padrão:
 * - @FileInInfo(): Construtor padrão que é usado para inicializar uma instância da estrutura `FileInInfo`.
 *   Não requer argumentos adicionais e usa o construtor padrão de `ErrorData`.
 */
struct FileInInfo {
    std::string filename;

    struct ErrorData {
        enum class ErrorType {
            WARNING,
            ERROR,
            FATAL
        };

        ErrorType errorType;
        std::string errorMessage;
        int errorCode;

        ErrorData() : errorType(ErrorType::WARNING), errorMessage(""), errorCode(0) {}
        ErrorData(ErrorType type, const std::string &message, int code)
                : errorType(type), errorMessage(message), errorCode(code) {}

    } errors;

    struct Event {
        std::function<void(const std::string &)> beforeRead;
        std::function<void(const std::string &)> afterRead;
        std::function<void(const ErrorData &)> onerror;
    } events;


    FileInInfo() = default;
};

/**
 * @brief Interface para leitura de arquivos.
 *
 * A interface `IFileReader` define operações básicas para a leitura de arquivos, como carregar um arquivo
 * e iniciar a execução do trabalho associado à leitura.
 *
 * Irá ter mais implementações, e Faz parte deo aprendizado e aplicações de técnicas conhecidas como Design by Contract
 *
 * @note Métodos:
 * - @loadFile: Método puro virtual que permite carregar um arquivo e associar um callback para manipular
 *   eventos relacionados à leitura.
 * - @startJob: Método puro virtual que inicia a execução do trabalho associado à leitura do arquivo.
 * -
 */
class IFileReader {
public:
    /**
     * @brief Método puro virtual para carregar um arquivo e associar um callback.
     *
     * Este método permite carregar um arquivo e associar um callback para manipular eventos relacionados à leitura,
     * como eventos antes da leitura, após a leitura e eventos de erro.
     *
     * @param callback Função que aceita uma referência para a estrutura `FileInInfo` como argumento, permitindo
     * manipular informações associadas ao arquivo.
     * @return Uma referência para a própria instância de `IFileReader` para suportar chamadas encadeadas.
     */
    virtual IFileReader &loadFile(const std::function<void(FileInInfo &)> &callback) = 0;

    /**
     * @brief Método puro virtual para iniciar a execução do trabalho associado à leitura do arquivo.
     *
     * Este método inicia a execução do trabalho associado à leitura do arquivo. A implementação concreta
     * deste método deve definir a lógica específica da leitura do arquivo.
     *
     * @return Uma referência para a própria instância de `IFileReader` para suportar chamadas encadeadas.
     */
    virtual IFileReader &startJob() = 0;
};

#endif //LEARN_C_ADVANCED_COURSE_IFILEREADER_H
